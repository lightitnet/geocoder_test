## Test solution to realize work with geocoder

![language:Python](https://img.shields.io/badge/Language-Python3.6-blue.svg)

To create this test script took about 2 hours for developer.

## Test task

You can see the task using [this link](https://docs.google.com/document/d/10q-McB-gF9kNW_WvCOTf2i8b0NAcZEf_sEewyoL3C74/)

## Result file

[The result file](https://drive.google.com/file/d/1IaxgX6xs-DGY_1Mz8ZFnPPTgfgVNYXUe/view?usp=sharing) with latitude and longtitude was gotten from geocoder

## Installation
Follow these steps to install the buildsim code on your computer:

1. Clone the repo with this command:

    * ```git clone https://bitbucket.org/lightitnet/geocoder_test.git```

2. Create and activate a virtual environment:

    * ```pip install virtualenv```
    * ```virtualenv -p python3 .env```
    * ```source .env/bin/activate```


3. Install needed packages:

    * ```pip install -r requirements.txt```

4. Put csv file in root folder of script with name: "Parking_Violations_Issued_-Fiscal_Year_2018.csv".

5. Changing key value in main.py

## Running

In the root folder of your cloned repo at the command prompt, run the command ```python main.py```.

Default file name is "Parking_Violations_Issued_-Fiscal_Year_2018.csv" that should be in this root folder. This file will be changed after getting data from geocoder to save values of next columns: "House number", "Street Name", "Intersecting Street", "latitude" and "longtitude".

As google geocoder has limit we couldn't use it for 50000 rows. Instead of google we use mapquest with own key. It's no problem to change this geocoder to google geocoder changing script row from ```geocoder.mapquest``` to ```geocoder.google```. That's why we chose this library.

## Improving

If you need we can improve script adding ArgParser (if you need to use script in terminal with different options), remembering all data from files (don't rewrite it), etc. Also we use variable to exclude repeated requests to geocode service as csv file has the same address. 

To get more quickly result we can create asyncronous work and use NoSql storage to manage exist_coords globally.