import geocoder
import pandas as pd


KEY = "n6ROGSxWKZ2lCoxayldpD9XakesooyE3"


class Coord:

    # To exclude repeated requests
    exist_coords = {}
    index = 0

    def _get_exist_coords(self, address):
        return self.exist_coords.get(address)

    def _save_new_coords(self, address, latlng):
        self.exist_coords[address] = latlng

    def get_coord(self, item):
        self.index += 1
        # If we only talk about NY
        address = " ".join(str(i) for i in item.values if not pd.isna(i)) + ", NY"

        # Looking for existing result
        res = self._get_exist_coords(address)

        # If we didn't find data then we are looking for internet
        if not res:
            res = geocoder.mapquest(address, key=KEY).latlng
            self._save_new_coords(address, res)

        print(self.index, address, res)
        if not res:
            return
        return pd.Series(res)


def start(csv_name="Parking_Violations_Issued_-Fiscal_Year_2018.csv"):
    # Read data from csv
    columns = ("House Number", "Street Name", "Intersecting Street")
    data = pd.read_csv(csv_name, usecols=columns, nrows=5000)

    # Looking for coords from geocoder
    handler_coord = Coord()
    data[["latitude", "longitude"]] = data.apply(handler_coord.get_coord, axis=1)

    # Save updated data to the same csv
    data.to_csv(csv_name, index=False)


if __name__ == "__main__":
    start()
